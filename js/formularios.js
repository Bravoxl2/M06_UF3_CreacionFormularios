contadorPregunta = 0;
contadorOpciones = 1;
contadorFormulario = 1;
arrayFormularios = [];
editar = false;

window.onload = function () {
    let btn = document.getElementById("btnCrearFormulario");
    btn.addEventListener("click", addFormulario);
};

function addFormulario(e) {
    e.preventDefault();
    let pregunta = document.getElementById('pregunta0');

    let div = document.getElementById("creadorForm");
    let inputElements = div.querySelectorAll("input");
    //Comprobamos que para crear formulario tenga almenos una pregunta creada
    if (pregunta != null) {
        let form = document.getElementById("formulario");
        let btnEditar = document.createElement("button");
        let btnBorrar = document.createElement("button");
        let div = document.getElementById('formCreado');
        let titulo = inputElements[0];

        btnEditar.addEventListener("click", editarFormulario)
        btnEditar.setAttribute("id", "btnEditar" + contadorFormulario);
        btnEditar.innerHTML = "Editar";
        
        btnBorrar.setAttribute("id", "btnBorrar")
        btnBorrar.addEventListener("click", borrarFormulario)
        btnBorrar.innerHTML = "Borrar";
        btnBorrar.disabled = true;

        div = form.cloneNode(true);
        div.setAttribute("id", "formulario" + contadorFormulario);
        for (let i = 0; i < div.length; i++) {
            if (div[i].disabled == false) { //Bloqueo de los input
                div[i].disabled = true;
            }

            let input = div.getElementsByTagName("input")[i];
            if (input != undefined) {
                input.disabled = true;
            }

            if (div[i].getAttribute("id") == 'btnCrearFormulario' ) {
                div[i].remove();
            }

        }
        if (!arrayFormularios.includes(titulo.value)) {
            arrayFormularios.push(titulo.value);
            div.appendChild(btnBorrar);
            div.appendChild(btnEditar);
            document.getElementById('formCreado').appendChild(div);
            contadorFormulario++;
            resetCrearForm();
        } else {
            alert("Ya existe un formulario con ese nombre");
        }
    } else alert('Hace falta crear almenos una pregunta');

}

function editarFormulario(e) {
    //Aqui se deberían de desbloquear y una vez que se guarde volver a bloquear los inputs.
    e.preventDefault();
    let div = document.getElementById(e.target.parentElement.id);
    
    editar = !editar;
    if (editar == true) {
        e.target.innerHTML = "Guardar";
    } else e.target.innerHTML = "Editar";

    for (var i = 0; i < div.length - 1; i++) {
        div[i].disabled = !div[i].disabled;
        if (div[i].getAttribute("class") == 'btnAdd') {
            div[i].addEventListener("click", addOpcion);
        }

        let select = div.getElementsByTagName('select')[i];

        if (select != undefined) {

            select.addEventListener('change', (event) => {
                selectChangeTipo(select.value, select.id);
            })
        }
    }

}

function addPregunta(element) {
    contadorOpciones = 1;
    let div = document.forms[0];
    let divCreado = document.createElement('div');

    divCreado.setAttribute("id", "pregunta" + contadorPregunta);
    let pregunta = document.createElement('input');
    pregunta.setAttribute("type", "text");

    div.appendChild(divCreado);
    divCreado.appendChild(pregunta);
    pregunta.placeholder = "Título pregunta";
    crearSelect(); //Crea el select para la pregunta añadida
    crearPregunta(); //Crear formulario con opciones
    contadorPregunta++;
}

function resetCrearForm() {
    let div = document.getElementById('creadorForm');
    let form = document.getElementById('formulario');
    while (form.firstChild) {
        form.removeChild(form.firstChild);
    }

    let input = document.createElement('input');
    input.setAttribute("id", "tituloFormulario");
    input.setAttribute("placeholder", "Titulo Formulario");

    let button = document.createElement('button');
    button.setAttribute("id", "btnCrearFormulario");
    button.innerHTML = 'Crear Formulario';
    button.addEventListener("click", addFormulario);

    form.appendChild(input);
    form.appendChild(button);
    div.insertBefore(form, div);

}

function borrarFormulario(e){
    e.preventDefault();
    contadorPregunta = 0;
    let formDelete = e.target.parentElement;
    formDelete.remove();
}

function selectChangeTipo(valueRecibido, preguntaRecibida) {
    let res;

    switch (valueRecibido) {
        case "Input Password":
            res = "password";
            break;
        case "Input Text":
            res = "text";
            break;
        case "Input Data":
            res = "data";
            break;
        case "Input Range":
            res = "range";
            break;
        case "Input File":
            res = "file";
            break;
        case "Input Email":
            res = "email";
            break;
        case "Radio":
            res = "radio";
            break;
        case "CheckBox":
            res = "checkbox";
            break;
        case "Selecion Simple":
            res = "radio";
            break;
        case "Selecion Multiple":
            res = "checkbox";
            break;
    }

    let inputs = document.getElementById(preguntaRecibida).getElementsByTagName("input");
    for (let i = 1; i < inputs.length; i++) {
        inputs[i].setAttribute("type", res);
    }


}

function crearSelect() {
    let pregunta = document.getElementById("pregunta" + contadorPregunta);
    let selectTipo = document.createElement("select");
    selectTipo.setAttribute("id", "pregunta" + contadorPregunta);
    let optionInputPsw = document.createElement("option");
    optionInputPsw.setAttribute("value", "Input Password");
    optionInputPsw.innerHTML = "Input Password";
    optionInputPsw.setAttribute("data-content", "<i class='fa-solid fa-plus' aria-hidden='true'></i>Option1")

    let optionInputText = document.createElement("option");
    optionInputText.setAttribute("value", "Input Text");
    optionInputText.innerHTML = "Input Text";


    let optionInputData = document.createElement("option");
    optionInputData.setAttribute("value", "Input Data");
    optionInputData.innerHTML = "Input Data";

    let optionInputRange = document.createElement("option");
    optionInputRange.setAttribute("value", "Input Range");
    optionInputRange.innerHTML = "Input Range";

    let optionInputFile = document.createElement("option");
    optionInputFile.setAttribute("value", "Input File");
    optionInputFile.innerHTML = "Input File";

    let optionInputEmail = document.createElement("option");
    optionInputEmail.setAttribute("value", "Input Email");
    optionInputEmail.innerHTML = "Input Email";

    let optionRadio = document.createElement("option");
    optionRadio.setAttribute("value", "Radio");
    optionRadio.innerHTML = "Radio Buttons";

    let optionCheckBox = document.createElement("option");
    optionCheckBox.setAttribute("value", "CheckBox");
    optionCheckBox.innerHTML = "Checkbox Buttons";

    let optionSelecionSimple = document.createElement("option");
    optionSelecionSimple.setAttribute("value", "Selecion Simple");
    optionSelecionSimple.innerHTML = "Selecion Simple";

    let optionSelecionMultiple = document.createElement("option");
    optionSelecionMultiple.setAttribute("value", "Seleccion Multiple");
    optionSelecionMultiple.innerHTML = "Selecion Multiple";

    let br = document.createElement("br");

    selectTipo.appendChild(optionInputPsw);
    selectTipo.appendChild(optionInputText);
    selectTipo.appendChild(optionInputData);
    selectTipo.appendChild(optionInputRange);
    selectTipo.appendChild(optionInputFile);
    selectTipo.appendChild(optionInputEmail);
    selectTipo.appendChild(optionRadio);
    selectTipo.appendChild(optionCheckBox);
    selectTipo.appendChild(optionSelecionSimple);
    selectTipo.appendChild(optionSelecionMultiple);
    pregunta.appendChild(selectTipo);
    pregunta.appendChild(br);
    selectTipo.addEventListener('change', (event) => {
        selectChangeTipo(event.target.value, pregunta.getAttribute("id"));
    })
    return pregunta;
}

function addOpcion(e) {
    e.preventDefault();
    let div = e.target.parentElement;

    let labels = div.getElementsByTagName("label"); //Obtención de todos los labels
    let cont;


    if (labels.length == 0) { //Si se han borrado todas las opciones, se setea el contador a 1.
        cont = 1;
    } else {
        cont = labels[labels.length - 1].id; //Para obtener el último label y obtener su id.
        cont = (parseInt(cont.slice(-1)) + 1);
    }

    let label = document.createElement("label");
    label.innerHTML = "Opcion " + cont + ": ";
    label.setAttribute("id", "label" + cont);


    let opcion = document.createElement("input");
    opcion.setAttribute("type", "text");
    opcion.setAttribute("id", "input" + cont);

    let br = document.createElement("br");
    br.setAttribute("id", "br" + cont);

    let btnDelOpcion = document.createElement("button");
    btnDelOpcion.addEventListener("click", delOpcion);
    btnDelOpcion.setAttribute("id", "btn" + cont);
    btnDelOpcion.innerHTML = "X";
    btnDelOpcion.setAttribute("class", "btnDel");

    div.insertBefore(label, div.children[(div.childNodes.length - 1)]);
    div.insertBefore(opcion, div.children[(div.childNodes.length - 1)]);
    div.insertBefore(btnDelOpcion, div.children[(div.childNodes.length - 1)]);
    div.insertBefore(br, div.children[(div.childNodes.length - 1)]);

}

function delOpcion(e) {
    e.preventDefault();

    let cont = (parseInt((e.target.id).slice(-1)));  //Para obtener el último label y obtener su id.
    //delete label and input with id cont 
    let div = e.target.parentElement;
    //remove label and input with id cont of this div
    div.removeChild(document.getElementById("label" + cont));
    div.removeChild(document.getElementById("input" + cont));
    div.removeChild(document.getElementById("btn" + cont));
    div.removeChild(document.getElementById("br" + cont));
}

function crearPregunta() {
    let br = document.createElement("br");
    br.setAttribute("id", "br" + contadorOpciones);
    let div = document.getElementById("pregunta" + contadorPregunta);
    let opcion = document.createElement("input");
    opcion.setAttribute("type", "text");
    opcion.setAttribute("id", "input" + contadorOpciones);

    let btnAddOpcion = document.createElement("button");
    btnAddOpcion.addEventListener("click", addOpcion);
    btnAddOpcion.innerHTML = "Nueva Opción";
    btnAddOpcion.setAttribute("class", "btnAdd");

    let btnDelOpcion = document.createElement("button");
    btnDelOpcion.setAttribute("id", "btn" + contadorOpciones);
    btnDelOpcion.addEventListener("click", delOpcion);
    btnDelOpcion.innerHTML = "X";
    btnDelOpcion.setAttribute("class", "btnDel");

    let label = document.createElement("label");
    label.innerHTML = "Opcion " + contadorOpciones + ": ";
    label.setAttribute("id", "label" + contadorOpciones);
    div.appendChild(label);
    div.appendChild(opcion);
    div.appendChild(btnDelOpcion);
    div.appendChild(br);
    div.appendChild(btnAddOpcion);
    contadorOpciones++;
}





